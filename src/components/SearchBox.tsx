import * as React from "react";
import { GoogleBooksApi } from "../api/GoogleBooksApi"
import { BookVolumesResult } from "../api/BookVolumesResult"
import { SearchInput } from "./SearchInput"
import { SearchResult } from "./SearchResult"
import { Pager } from "./Pager"

export interface SearchBoxProps { 
}

export interface SearchBoxState { 
    page?: number,
    entriesPerPage?: number;
    query?: string;
    foundBooks?: BookVolumesResult;
}

export class SearchBox extends React.Component<SearchBoxProps, SearchBoxState> {

    constructor() {
        super();
        this.state = {
            page: 0,
            entriesPerPage: 10,
            query: "flowers",
            foundBooks: {totalItems: null, items: []}
        };    
    }

    search = (query: string) => {
        this.setState({
            query: query,
            page: 0
        });
        this.searchCore(query, 0);
    };

    nextPage = () => {
        const newPage = this.state.page + 1;
        this.setState({page: newPage});
        this.searchCore(this.state.query, newPage);
    };

    private async searchCore(query: string, page: number) {
        const result = await GoogleBooksApi.find(query, page*this.state.entriesPerPage, this.state.entriesPerPage);
        this.setState({ foundBooks: result });
        console.log(result);        
    }

    render() {

        const books = this.state.foundBooks.items.map((item, index, collection) => <p>
            <em>{item.volumeInfo.title}</em> 
        </p>);

        return <div>
            <div>
                <SearchInput initialValue={this.state.query} searchCallback={this.search} />
            </div>
            <Pager value={this.state.foundBooks} page={this.state.page} forwardCallback={this.nextPage} />
            {this.state.foundBooks.totalItems > 0 &&
            <div>
                {this.state.foundBooks.items.map(item => 
                    <SearchResult key={item.id} value={item} />)}
            </div>
            }
        </div>;
    }
}