import * as React from "react";

export interface SearchInputProps { 
    initialValue: string;
    searchCallback: (v: string) => void; 
}

export interface SearchInputState { 
    value: string;
}

export class SearchInput extends React.Component<SearchInputProps, SearchInputState> {

    constructor(props: SearchInputProps) {
        super(props);
        this.state = {
            value: props.initialValue
        };
    }

    updateState = (event: React.FormEvent<any>) => {
        this.setState({value: event.currentTarget.value});
    };

    keyPressed = (event: React.KeyboardEvent<any>) => {
        if (event.charCode !== 13) return;
        this.startSearch();
    };

    searchClicked = (event: React.FormEvent<any>) => {
        this.startSearch();
    };

    private startSearch() {
        this.props.searchCallback(this.state.value);
    }

    render() {
        return  <div>
                    <input type="text" value={this.state.value} onKeyPress={this.keyPressed} onChange={this.updateState} />
                    <button onClick={this.searchClicked}>Find</button>
                </div>;
    }
}