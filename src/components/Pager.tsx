import * as React from "react";
import { BookVolumesResult } from "./../api/BookVolumesResult";

export interface PagerProps { 
    value: BookVolumesResult;
    page: number;
    forwardCallback: () => void;
}

export interface PagerState { }

export class Pager extends React.Component<PagerProps, PagerState> {

    nextClicked = () => {
        this.props.forwardCallback();
    };

    render() {
        return <div>
            {this.props.value.totalItems == null &&
                <div>Please enter query string and press `Find` button</div>
            }
            {this.props.value.totalItems === 0 &&
                <div>Not found</div>            
            }
            {this.props.value.totalItems > 0 &&            
                <div>
                    Found: {this.props.value.totalItems} book(s); Page {this.props.page + 1} 
                    <button onClick={this.nextClicked}>Next &rarr;</button>
                </div>
            }
            </div>;
    }
}