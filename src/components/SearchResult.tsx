import * as React from "react";
import { BookVolumeResult } from "./../api/BookVolumeResult";

export interface SearchResultProps { 
    value: BookVolumeResult;
}

export interface SearchResultState { }

export class SearchResult extends React.Component<SearchResultProps, SearchResultState> {

    render() {
        return  <div>
            {this.props.value.volumeInfo.imageLinks && this.props.value.volumeInfo.imageLinks.smallThumbnail &&
                <div><img src={this.props.value.volumeInfo.imageLinks.smallThumbnail} /></div>
            }
            <div>
            <strong>{this.props.value.volumeInfo.title}</strong>
            {this.props.value.volumeInfo.authors && this.props.value.volumeInfo.authors.length > 0 &&
                <span> by {this.props.value.volumeInfo.authors.join(", ")}</span>
            }
            <br/>
            {this.props.value.searchInfo && 
                this.props.value.searchInfo.textSnippet
            }
            <br/>
            </div>
        </div>;
    }
}