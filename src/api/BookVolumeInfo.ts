
import { ImageLinks } from "./ImageLinks";

export interface BookVolumeInfo {
    title: string;
    authors?: string[];
    imageLinks?: ImageLinks;
}