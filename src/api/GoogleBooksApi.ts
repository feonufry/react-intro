
export class GoogleBooksApi {
    public static find(query: string, startIndex: number, maxResults: number): Promise<any> {
        return fetch(`https://www.googleapis.com/books/v1/volumes?q=${query}&startIndex=${startIndex}&maxResults=${maxResults}`)
            .then(response => response.json());
    }
}