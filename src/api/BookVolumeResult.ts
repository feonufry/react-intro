
import { BookVolumeSearchInfo } from "./BookVolumeSearchInfo";
import { BookVolumeInfo } from "./BookVolumeInfo";

export interface BookVolumeResult {
    id: string;
    selfLink: string;
    searchInfo: BookVolumeSearchInfo;
    volumeInfo: BookVolumeInfo;
}