
import { BookVolumeResult } from "./BookVolumeResult";

export interface BookVolumesResult {
    totalItems: number;
    items: BookVolumeResult[];
}