import { AppContainer } from "react-hot-loader";
import * as React from "react";
import * as ReactDOM from "react-dom";

import { App } from "./App";

const hostNode = document.getElementById("shell"); 
ReactDOM.render(
    <AppContainer>
        <App />
    </AppContainer>,
    hostNode
);

if (module.hot) {
    let update = () => {
    // If you use Webpack 2 in ES modules mode, you can
    // use <App /> here rather than require() a <NextApp />.
    const NextApp = require("./App").default;
    ReactDOM.render(
      <AppContainer>
         <NextApp />
      </AppContainer>,
      hostNode
    );
  };

  //module.hot.accept("./App", update);
  module.hot.accept();
}
